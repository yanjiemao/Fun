
## Fun观影娱乐小程序
### 主要内容：
     1.视频的在线播放
     2.在线视频分类、搜索
     3.视频详情中可选择观看画质和选集
     4.在引用微信帐号登录，记录我的收藏与观看记录
     5.观影之余看看笑话
---
##  项目展示图片



<img src="readme_pic/index.png" width="30%" /><img src="readme_pic/movieAction.png" width="30%" /><img src="readme_pic/search.png" width="30%" />


<img src="readme_pic/movieDetails.png" width="30%" /><img src="readme_pic/movieOnPlay.png" width="30%" /><img src="readme_pic/meLogin.png" width="30%" />

<img src="readme_pic/meLogined.png" width="30%" /><img src="readme_pic/history.png" width="30%" /><img src="readme_pic/collect.png" width="30%" />

<img src="readme_pic/location.png" width="30%" /><img src="readme_pic/chooseLocation.png" width="30%" /><img src="readme_pic/joke.png" width="30%" />

---

