
// 引入SDK核心类
var QQMapWX = require('./qqmap-wx-jssdk.js');
var qqmapsdk = new QQMapWX({
  key: 'R5OBZ-WF53U-M7LV6-2UUG3-N3WI3-JZF4Q'
  
});

Page({
  data: {
    longitude: 0,
    latitude: 0,
    address:'',
    markers:[],
    nickName:'',
    avatarUrl:'',
  },
  geoAddress(lati, long) {
    var that = this;
    qqmapsdk.reverseGeocoder({
      location: {
        latitude: lati,
        longitude: long
      },
      success: function (addressRes) {
        console.log(addressRes);
        var address = addressRes.result.address;
        that.setData({
          address: address,
        })
      },
      fail: function (res) {
        console.log(res);
        wx.showToast({
          title: '解析地址错误',
          icon: 'loading',
          duration: 1000
        });

      },

    })
  },

  
  choose:function(){
    wx.chooseLocation({
     
      success: function(res) {
       
      },
    })
  },
  requestLocation:function(){
    var that = this;
    wx.getLocation({
      type: 'gcj02',
      success: function (res) {
        console.log(res);
        const latitude = res.latitude
        const longitude = res.longitude
        console.log(longitude);
        that.geoAddress(latitude, longitude);
        that.setData({
          longitude: longitude,
          latitude: latitude,
          markers: [{
            iconPath: '../../icons/campus_location.png',
            id: 0,
            width: 32,
            height: 32,
            latitude: latitude,
            longitude: longitude,
            title: '我的位置',
          }]
        });
      },
    })
  },
  
  onLoad: function () {
    // 实例化API核心类
  },

  onShow:function(){
    this.requestLocation();
    wx.removeTabBarBadge({
      index: 3,
    })
  }

})

